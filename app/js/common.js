jQuery(document).ready(function($) {
	// Tabs initialization
	$( "#tabs" ).tabs();

	//Toogle Footer Menu
	$('.footer-mnu ul li.menu-item-has-children > a').on( 'click', function() {
		$(this).next('.sub-menu').toggle(300);
		$(this).toggleClass('active');
		return false;
	});

	//Initialization OwlCarousel
	$('.owl-home').owlCarousel({
		items: 1,
		loop:true,
		nav:true,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true,
		animateOut: 'rotateOutUpLeft'
	});
	//Slider Exposition
	$('.owl-exposition-slider').owlCarousel({
		margin:30,
		loop:true,
		nav:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:4
			}
		}
	});

	//Add attribute jquery
	$('.gallery-icon > a').attr('data-fancybox', 'images');

	//Initialization Fancybox
	$('[data-fancybox]').fancybox({
		animationEffect : 'fade'
	});

	//Initialization Maskedinput
	$('.phone-input').mask('+7 (999) 999-9999');

	//E-mail Ajax Send
	$('.callback').submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: 'POST',
			url: '/wp-content/themes/gallery/mail.php', //Change
			data: th.serialize()
		}).done(function() {
			alert('Сообщение отправлено!');
			jQuery.magnificPopup.close();
			setTimeout(function() {
				// Done Functions
				th.trigger('reset');
			}, 1000);
		});
		return false;
	});

	//Imitialization mmenu
	$('#mobile-mnu').mmenu({
		navbar: {
			title: 'Меню'
		},
		'navbars': [
			{
				'position': 'bottom',
				'content': [
					'<a href="#order-popup" class="btn open-popup-link"><span>Закажи экскурсию</span></a>'
				]
			}
		],
		'extensions': [
			'border-full',
			'fx-menu-slide',
			'theme-white'
		]
	});
	var API = $('#mobile-mnu').data( 'mmenu' );
	API.bind('open:finish', function(){
		$('#toggle-mobile-mnu').addClass('is-active');
	});
	API.bind('close:finish', function(){
		$('#toggle-mobile-mnu').removeClass('is-active');
	});

	// CALL FUNCTION REARRANGE ELEMENT TO DOM
	if( $("section").is("#mb01") && $("section").is("#mb02") ) {
		rearrange();
	}

	// Open popup
	$('.open-popup-link').magnificPopup({
		removalDelay: 300,
		type:'inline',
		mainClass: 'mfp-fade',
		closeBtnInside: true
	});

	//Кнопка "Наверх"
	//Документация:
	//http://api.jquery.com/scrolltop/
	//http://api.jquery.com/animate/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('#top').fadeIn();
		} else {
			$('#top').fadeOut();
		}
	});
	$("#top").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	
});

// REARRANGE ELEMENT TO DOM
function rearrange() {
	jQuery(window).resize(function() {
		var dw = jQuery(document).width();
		if (dw < 768) {
			var b1 = document.getElementById('mb01');
			var b2 = document.getElementById('mb02');
			b1.parentNode.insertBefore(b2, b1);
		}
		else {
			var b1 = document.getElementById('mb01');
			var b2 = document.getElementById('mb02');
			b1.parentNode.insertBefore(b1, b2);
		}
	});
};

// Hide Preloader
jQuery(window).on('load', function() {
	jQuery('.preloader').delay(400).fadeOut('slow');
})